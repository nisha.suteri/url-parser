package word.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

public class Controller {

	private static final int NUMBER_OF_CONSUMERS = 4;
	private static final int NUMBER_OF_PRODUCERS = 4;
	private static final int QUEUE_SIZE = 5;
	private static BlockingQueue<String> sharedQ;
	private static Collection<Thread> producerThreadCollection, allThreadCollection;
	private static volatile ConcurrentHashMap<String, Word> countMap = new ConcurrentHashMap<String, Word>();
	private static volatile ConcurrentHashMap<String, Word> pairMap = new ConcurrentHashMap<String, Word>();
	private static volatile Set<String> visited = new HashSet<String>();

	public static void main(String[] args) throws IOException, InterruptedException {
		producerThreadCollection = new ArrayList<Thread>();
		allThreadCollection = new ArrayList<Thread>();
		sharedQ = new LinkedBlockingDeque<String>(QUEUE_SIZE);
		if (args.length > 0) {
			System.out.println("Analysing....");
			createAndStartProducers(args[0]);
			createAndStartConsumers();

			for (Thread t : allThreadCollection) {
				try {
					t.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			SortedSet<Word> sortedWords = new TreeSet<Word>(countMap.values());
			int i = 0;
			int maxWordsToDisplay = 10;
			System.out.println("------Top 10 Frequent words------");
			for (Word word : sortedWords) {
				if (i >= maxWordsToDisplay) { // 10 is the number of words you want to show frequency for
					break;
				}

				System.out.println(word.word + "\t" + word.count);
				i++;

			}
			SortedSet<Word> sortedpairs = new TreeSet<Word>(pairMap.values());
			i = 0;
			System.out.println("------Top 10 Frequent pairs------");
			for (Word word : sortedpairs) {
				if (i >= maxWordsToDisplay) { // 10 is the number of words you want to show frequency for
					break;
				}

				System.out.println(word.word + "\t" + word.count);
				i++;

			}
		}
		else {
			System.out.println("pass URL to be analysed...");
		}
	}

	private static void createAndStartProducers(String args) throws InterruptedException {
		List<String> mainUrl = new ArrayList<String>();
		mainUrl.add(args);
		URLFinder urlFinder = new URLFinder(mainUrl, sharedQ, visited, 0);
		Thread producerThread = new Thread(urlFinder, "producer-" + 0);
		producerThread.start();
		producerThread.join();
		int each = urlFinder.links.size() / NUMBER_OF_PRODUCERS;
		int start = 0;
		for (int i = 1; i < NUMBER_OF_PRODUCERS; i++) {
			int end = 0;
			if (i == NUMBER_OF_PRODUCERS - 1)
				end = urlFinder.links.size() - 1;
			else
				end = (i * each) - 1;
			List<String> url = new ArrayList<String>(urlFinder.links.subList(start, end));
			start = i * each;
			URLFinder urls = new URLFinder(url, sharedQ, visited, 1);
			Thread pThread = new Thread(urls, "producer-" + i);
			producerThreadCollection.add(pThread);
			pThread.start();
		}
		allThreadCollection.addAll(producerThreadCollection);
	}

	private static void createAndStartConsumers() {
		for (int i = 1; i <= NUMBER_OF_CONSUMERS; i++) {
			Thread consumerThread = new Thread(new Consumer(sharedQ, countMap, pairMap), "consumer-" + i);
			allThreadCollection.add(consumerThread);
			consumerThread.start();
		}
	}

	public static boolean isProducerAlive() {
		for (Thread t : producerThreadCollection) {
			if (t.isAlive())
				return true;
		}
		return false;
	}

	public static class Word implements Comparable<Word> {
		String word;
		int count;

		@Override
		public int hashCode() {
			return word.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return word.equals(((Word) obj).word);
		}

		public int compareTo(Word b) {
			return b.count - count;
		}
	}
}
