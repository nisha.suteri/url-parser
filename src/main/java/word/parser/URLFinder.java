package word.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class URLFinder extends Thread {

	List<String> mainUrl;
	int counter;
	List<String> links = new ArrayList<String>();
	volatile Set<String> visited;
	BlockingQueue<String> sharedQ;


	public URLFinder(List<String> url, BlockingQueue<String> sharedQ, Set<String> visited, int counter) {
		super();
		this.mainUrl = url;
		this.sharedQ = sharedQ;
		this.visited = visited;
		this.counter = counter;
	}

	public URLFinder(List<String> url, BlockingQueue<String> sharedQ, Set<String> visited, int counter,
			List<String> links) {
		super();
		this.mainUrl = url;
		this.sharedQ = sharedQ;
		this.visited = visited;
		this.counter = counter;
		this.links = links;
	}

	@Override
	public void run() {
		for (String mainUrl : this.mainUrl) {
			getInternals(mainUrl, counter);
		}
	}

	public void getInternals(String mainUrl, int counter) {
		Document doc;
		try {
			if ((!visited.contains(mainUrl)) && counter <= 4) {
				//System.out.println(Thread.currentThread().getName() + " fetching URL: " + mainUrl);
				visited.add(mainUrl);
				sharedQ.put(mainUrl);
				doc = Jsoup.connect(mainUrl).data("query", "Java").userAgent("Mozilla").cookie("auth", "token")
						// .timeout(3000)
						.get();
				Elements elements = doc.select("a[href]");
				for (Element element : elements) {
					if (element.attr("href").contains(mainUrl)) {
						if (counter == 0) {
							links.add(element.attr("href"));
						} else {
							getInternals(element.attr("href"), counter+1);
						}
					}

				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

}
