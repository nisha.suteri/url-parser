package word.parser;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import word.parser.Controller.Word;

public class Consumer implements Runnable {
	private BlockingQueue<String> queue;
	volatile ConcurrentHashMap<String, Word> countMap = new ConcurrentHashMap<String, Word>();
	volatile ConcurrentHashMap<String, Word> pairMap = new ConcurrentHashMap<String, Word>();

	public Consumer(BlockingQueue<String> q, ConcurrentHashMap<String, Word> countMap,
			ConcurrentHashMap<String, Word> pairMap) {
		queue = q;
		this.countMap = countMap;
		this.pairMap = pairMap;
	}

	public void run() {
		BufferedReader reader;
		while (true) {
			String url = queue.poll();

			if ((!Controller.isProducerAlive()) && queue.isEmpty())
				return;

			if (url != null) {
				// System.out.println(Thread.currentThread().getName() + " processing line: " +
				// url);
				Document doc = null;
				try {
					doc = Jsoup.connect(url).get();

					// Get the actual text from the page, excluding the HTML
					String text = doc.body().text();

					// System.out.println("Analyzing text...");
					// Create BufferedReader so the words can be counted
					reader = new BufferedReader(
							new InputStreamReader(new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8))));
					String line;
					while ((line = reader.readLine()) != null) {
						// String[] words = line.split("[^A-ZÃ…Ã„Ã–a-zÃ¥Ã¤Ã¶]+");
						String[] words = line.split("\\W+");
						for (int i = 0; i < words.length; i++) {

							if ("".equals(words[i])) {
								continue;
							}

							Word wordObj = countMap.get(words[i]);
							if (wordObj == null) {
								wordObj = new Word();
								wordObj.word = words[i];
								wordObj.count = 0;
								countMap.put(words[i], wordObj);
							}

							wordObj.count++;
							if (i < words.length - 1) {
								Word pairObj = pairMap.get(words[i] + " " + words[i + 1]);
								if (pairObj == null) {
									pairObj = new Word();
									pairObj.word = words[i] + " " + words[i + 1];
									pairObj.count = 0;
									pairMap.put(words[i] + " " + words[i + 1], pairObj);
								}
								pairObj.count++;
							}
						}
					}
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}

		}
	}
}
